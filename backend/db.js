const mysql= require("mysql2")
const pool= mysql.createPool(
    {
        host:"mydb",
        user:"root",
        password:"root",
        database:"mydb",
        port:3306,
        // host:"localhost",
        // user:"root",
        // password:"root",
        // database:"classwork",

        waitForConnections:true,
        connectionLimit:10,
        queLimit:0
    }
)

module.exports=pool