const express= require("express")
const routerMovie= require("./movieservice")
const cors= require("cors")
const app= express()

app.use(cors("*"))
app.use(express.json())

app.use("/movie",routerMovie)

app.listen(4000,'0.0.0.0',()=>{
    console.log("your server started on port 4000")
})