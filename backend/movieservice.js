const express = require("express")
const db= require("./db")
const utils= require("./utils")

const router= express.Router()

router.post("/get",(request,response)=>{
    const {movie_title}= request.body

    const statement= `select * from movie where movie_title = "${movie_title}";`
    db.execute(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
   
})


router.post("/addmovie",(request,response)=>{
    const {movie_title,movie_release_date,movie_time,director_name}= request.body

    const statement= `insert into movie (movie_title,movie_release_date,movie_time,director_name) values ("${movie_title}","${movie_release_date}","${movie_time}","${director_name}");`
    db.execute(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
   
})


router.post("/update",(request,response)=>{
    const {movie_title,movie_release_date,movie_time}= request.body

    const statement= `update movie set  
    movie_release_date= "${movie_release_date}",movie_time="${movie_time}"
     where movie_title= "${movie_title}";`
    db.execute(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
   
})

router.delete("/delete",(request,response)=>{
    const {movie_title}= request.body

    const statement= `delete from movie
     where movie_title= "${movie_title}";`
    db.execute(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
   
})

module.exports=router